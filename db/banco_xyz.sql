--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-4)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: actualizar_saldo(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.actualizar_saldo() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
	temp_saldo_c cuenta.saldo%TYPE;
	temp_saldo_s transaccion.saldo_cuenta%TYPE;
BEGIN
	temp_saldo_c := saldo FROM cuenta WHERE id = NEW.id_cuenta;
	IF (NEW.tipo_transaccion = 'consignacion') THEN
		temp_saldo_c := temp_saldo_c + NEW.monto;
	ELSIF (NEW.tipo_transaccion = 'retiro') THEN
		temp_saldo_c := temp_saldo_c - NEW.monto;
	ELSE	
	END IF;
	BEGIN
		temp_saldo_s := temp_saldo_c;
		UPDATE cuenta SET saldo = temp_saldo_c WHERE cuenta.id = NEW.id_cuenta;
		UPDATE transaccion SET saldo_cuenta = temp_saldo_s WHERE transaccion.id = NEW.id;
	END;
	RETURN NULL;
END;$$;


--
-- Name: confirmar_saldo(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.confirmar_saldo() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
	temp_saldo cuenta.saldo%TYPE;
BEGIN
	temp_saldo := saldo FROM cuenta WHERE id = NEW.id_cuenta;
	IF (NEW.tipo_transaccion = 'retiro') THEN
		IF (NEW.monto > temp_saldo) THEN
                        RAISE EXCEPTION 'Sin saldo suficiente.';
			RETURN NULL;
		END IF;
	END IF;
	RETURN NEW;
END;$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ciudad; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ciudad (
    id integer NOT NULL,
    nombre_ciudad character varying(255) NOT NULL,
    id_pais integer NOT NULL
);


--
-- Name: auto_incre_id_ciudad; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.auto_incre_id_ciudad
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auto_incre_id_ciudad; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.auto_incre_id_ciudad OWNED BY public.ciudad.id;


--
-- Name: auto_incre_id_cuenta; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.auto_incre_id_cuenta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auto_incre_id_pais; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.auto_incre_id_pais
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auto_incre_id_pais; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.auto_incre_id_pais OWNED BY public.ciudad.id_pais;


--
-- Name: auto_incre_id_transaccion; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.auto_incre_id_transaccion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cliente (
    tipo_documento character varying(255) NOT NULL,
    documento character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL,
    apellido character varying(255) NOT NULL,
    id_ciudad integer,
    clave integer DEFAULT 0,
    role character varying(255) DEFAULT 'usuario'::character varying,
    CONSTRAINT tipo_documento_chk CHECK ((((tipo_documento)::text = 'CC'::text) OR ((tipo_documento)::text = 'CE'::text) OR ((tipo_documento)::text = 'PA'::text) OR ((tipo_documento)::text = 'RC'::text) OR ((tipo_documento)::text = 'NIT'::text)))
);


--
-- Name: cuenta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cuenta (
    id integer DEFAULT nextval('public.auto_incre_id_cuenta'::regclass) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    saldo numeric NOT NULL,
    tipo_documento_cliente character varying NOT NULL,
    documento_cliente character varying NOT NULL
);


--
-- Name: pais; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pais (
    id integer DEFAULT nextval('public.auto_incre_id_pais'::regclass) NOT NULL,
    nombre_pais character varying NOT NULL
);


--
-- Name: telefonos_cliente; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.telefonos_cliente (
    id integer NOT NULL,
    numero_telefonico numeric NOT NULL,
    tipo_documento_cliente character varying(255) NOT NULL,
    documento_cliente character varying(255) NOT NULL,
    tipo_telefono character varying(255) NOT NULL,
    CONSTRAINT tipo_telefono_chk CHECK ((((tipo_telefono)::text = 'personal'::text) OR ((tipo_telefono)::text = 'trabajo'::text) OR ((tipo_telefono)::text = 'hogar'::text)))
);


--
-- Name: telefonos_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.telefonos_cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: telefonos_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.telefonos_cliente_id_seq OWNED BY public.telefonos_cliente.id;


--
-- Name: transaccion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaccion (
    id integer DEFAULT nextval('public.auto_incre_id_transaccion'::regclass) NOT NULL,
    fecha timestamp with time zone NOT NULL,
    monto numeric DEFAULT 0 NOT NULL,
    tipo_transaccion character varying NOT NULL,
    id_cuenta integer NOT NULL,
    saldo_cuenta numeric DEFAULT 0 NOT NULL,
    CONSTRAINT tipo_transaccion_chk CHECK ((((tipo_transaccion)::text = 'consulta'::text) OR ((tipo_transaccion)::text = 'retiro'::text) OR ((tipo_transaccion)::text = 'consignacion'::text)))
);


--
-- Name: ciudad id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ciudad ALTER COLUMN id SET DEFAULT nextval('public.auto_incre_id_ciudad'::regclass);


--
-- Name: telefonos_cliente id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telefonos_cliente ALTER COLUMN id SET DEFAULT nextval('public.telefonos_cliente_id_seq'::regclass);


--
-- Data for Name: ciudad; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.ciudad (id, nombre_ciudad, id_pais) FROM stdin;
1	bogota	1
2	ibague	1
3	cali	1
4	medellin	1
5	barranquilla	1
6	cartagena	1
7	bucaramanga	1
\.


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.cliente (tipo_documento, documento, nombre, apellido, id_ciudad, clave, role) FROM stdin;
CC	12349876	LIZETH	CERVERA	1	1234	usuario
CC	123456789	JORGE	RODRIGUEZ	1	1234	admin
CC	14136943	WILLIAM	HUERTAS	2	1015	admin
\.


--
-- Data for Name: cuenta; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.cuenta (id, fecha_creacion, saldo, tipo_documento_cliente, documento_cliente) FROM stdin;
2	2018-06-26 21:00:00-05	26500000	CC	12349876
1	2018-07-14 17:00:00-05	1000000	CC	14136943
3	2018-06-30 17:00:00-05	25000000	CC	14136943
13	2020-06-17 11:00:00-05	8000000	CC	123456789
14	2018-06-15 17:00:00-05	1500000	CC	123456789
\.


--
-- Data for Name: pais; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.pais (id, nombre_pais) FROM stdin;
1	colombia
2	estados unidos
3	francia
4	inglaterra
5	china
6	españa
\.


--
-- Data for Name: telefonos_cliente; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.telefonos_cliente (id, numero_telefonico, tipo_documento_cliente, documento_cliente, tipo_telefono) FROM stdin;
1	3147891278	CC	123456789	personal
2	3205894796	CC	12349876	personal
4	38654117	CC	12349876	hogar
5	38758127	CC	123456789	hogar
6	3172898060	CC	14136943	personal
7	32453127	CC	14136943	hogar
8	3218972365	CC	123456789	trabajo
9	3215647895	CC	14136943	trabajo
\.


--
-- Data for Name: transaccion; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaccion (id, fecha, monto, tipo_transaccion, id_cuenta, saldo_cuenta) FROM stdin;
3	2018-06-30 17:00:00-05	0	consulta	1	0
7	2018-07-14 17:00:00-05	1000000	retiro	1	0
1	2018-06-27 21:00:00-05	500000	retiro	2	0
2	2018-06-15 17:00:00-05	8000000	consignacion	2	0
4	2018-06-15 17:00:00-05	500000	retiro	2	0
5	2018-06-27 21:00:00-05	500000	retiro	2	0
14	2018-06-26 21:00:00-05	0	consulta	2	0
15	2018-06-15 17:00:00-05	0	consulta	2	26500000
16	2018-06-26 17:00:00-05	3000000	retiro	3	5000000
17	2018-06-27 21:00:00-05	20000000	consignacion	3	25000000
18	2020-06-18 17:00:00-05	500000	retiro	14	1500000
\.


--
-- Name: auto_incre_id_ciudad; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auto_incre_id_ciudad', 7, true);


--
-- Name: auto_incre_id_cuenta; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auto_incre_id_cuenta', 14, true);


--
-- Name: auto_incre_id_pais; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auto_incre_id_pais', 6, true);


--
-- Name: auto_incre_id_transaccion; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.auto_incre_id_transaccion', 19, true);


--
-- Name: telefonos_cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.telefonos_cliente_id_seq', 9, true);


--
-- Name: ciudad ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (id);


--
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (tipo_documento, documento);


--
-- Name: cuenta cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_pkey PRIMARY KEY (id);


--
-- Name: pais pais_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);


--
-- Name: telefonos_cliente telefonos_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telefonos_cliente
    ADD CONSTRAINT telefonos_cliente_pkey PRIMARY KEY (id);


--
-- Name: transaccion transaccion_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT transaccion_pkey PRIMARY KEY (id);


--
-- Name: transaccion tg_actualizacion_saldo; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tg_actualizacion_saldo AFTER INSERT ON public.transaccion FOR EACH ROW EXECUTE FUNCTION public.actualizar_saldo();
-- CREATE TRIGGER tg_actualizacion_saldo AFTER INSERT ON public.transaccion FOR EACH ROW EXECUTE PROCEDURE public.actualizar_saldo();

--
-- Name: transaccion tg_confirmar_saldo; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tg_confirmar_saldo BEFORE INSERT ON public.transaccion FOR EACH ROW EXECUTE FUNCTION public.confirmar_saldo();
-- CREATE TRIGGER tg_confirmar_saldo BEFORE INSERT ON public.transaccion FOR EACH ROW EXECUTE PROCEDURE public.confirmar_saldo();

--
-- Name: ciudad ciudad_id_pais_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_id_pais_fkey FOREIGN KEY (id_pais) REFERENCES public.pais(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cliente cliente_ciudad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_ciudad_fkey FOREIGN KEY (id_ciudad) REFERENCES public.ciudad(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cuenta cuenta_tipo_documento_cliente_documento_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_tipo_documento_cliente_documento_cliente_fkey FOREIGN KEY (tipo_documento_cliente, documento_cliente) REFERENCES public.cliente(tipo_documento, documento) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: telefonos_cliente telefonos_cliente_tipo_documento_cliente_documento_cliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telefonos_cliente
    ADD CONSTRAINT telefonos_cliente_tipo_documento_cliente_documento_cliente_fkey FOREIGN KEY (tipo_documento_cliente, documento_cliente) REFERENCES public.cliente(tipo_documento, documento) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaccion transaccion_id_cuenta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT transaccion_id_cuenta_fkey FOREIGN KEY (id_cuenta) REFERENCES public.cuenta(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

