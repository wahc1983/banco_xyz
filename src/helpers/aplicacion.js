// funcion para pasar a mayuscula la primera letra de una cadena
exports.toSentenceCase = (string) => {
	string = string.toLowerCase();
	let result = string.charAt(0).toUpperCase() + string.slice(1);
  return result;
};

// funcion para dar formato a la fecha
exports.formatDate = (x, y) => {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
};

// funcion para dar formato de moneda a un numero
exports.formatCurrency = (locales, currency, fractionDigits, number) => {
  var formatted = new Intl.NumberFormat(locales, {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: fractionDigits
  }).format(number);
  return formatted.replace('$',currency);
};