// se cargan las librerias necesarias
const { Client } = require('pg');
const settings = require('../settings');
const conector = module.exports;

// se cargan las variables de ambiente
var user = settings.pgUser;
var pass = settings.pgPass;
var dbName = settings.pgDbName;
var dbPort = settings.pgDbPort;

// se crea un cliete para la base de datos en postgres
const pgClient = new Client({
    connectionString: `postgresql://${user}:${pass}@localhost:${dbPort}/${dbName}`
});
pgClient.connect()
conector.pgConector = pgClient;

