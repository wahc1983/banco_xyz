require('dotenv').config();
const settings = module.exports;

settings.port = process.env.PORT;

settings.host = process.env.HOST;

settings.pgUser = process.env.PG_USER;

settings.pgPass = process.env.PG_PASS;

settings.pgDbName = process.env.PG_DB_NAME;

settings.pgDbPort = process.env.PG_DB_PORT;

