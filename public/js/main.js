$(document).ready(function() {

	// captura el evento change en el selector de cuenta para activar selector de transaccion
	$('#selector_cuenta').on('change', function(){
		limpiarMensajesTransaccion();
		$('#monto').prop("disabled", true).val('');
		$('#tipo_transaccion').prop("disabled", false).val('');
		$('#btn_enviar').prop("disabled", true);
	});

	// captura el evento change en el selector de transaccion para verificar los fondos en la cuenta
	// y activar el input monto.
	$('#tipo_transaccion').on('change', function(){
		limpiarMensajesTransaccion();
		$('#monto').val('');
		let montoTransaccion = $('#monto');
		let tipoTransaccion = $(this).val();
		if (tipoTransaccion == "retiro" || tipoTransaccion == "consignacion") {
			montoTransaccion.prop("disabled", false);
			$('#btn_enviar').prop("disabled", true);
			if (tipoTransaccion == "retiro") {comprobarSaldo('changeSelectTransaccion')};
		}else{
			montoTransaccion.prop("disabled", true);
			$('#btn_enviar').prop("disabled", false);
		};
	});

	// captura el evento keyup en el input monto para verificar los fondos en la cuenta y activar 
	// el bonto de envio
	comprobarSaldoTimer = null;

	$('#monto').on('keyup', function(){
		let tipoTransaccion = $('#tipo_transaccion').val();
		if (tipoTransaccion == 'retiro') {
			let monto = $(this).val();
			if (monto != "") {
				clearTimeout(comprobarSaldoTimer);
				comprobarSaldoTimer = setTimeout(comprobarSaldo, 1000, 'keyup');
			}else{
				clearTimeout(comprobarSaldoTimer);
			};
		}else{
			let monto = $(this).val();
			if (monto != "") {
				$('#btn_enviar').prop("disabled", false)
			}else{
				$('#btn_enviar').prop("disabled", true)
			}
		};
	});

	// captura el evento change en el selector de pais para obtener las ciudades.
	$('#pais_nuevo').on('change', function(){
		let pais_id = $(this).val();
		if (pais_id != '') {
			obtenerCiudades(pais_id);
		}else{
			$('#ciudad_nuevo').prop('disabled', true).val('')
		};
	});

	//
	tables_scrollables_containers = $('.table_scrollable_container');
	$(window).resize(function() {
			tables_scrollables_containers.each(function(){
				respondToVisibility(this, visible => {if(visible) {resizeTable()}});
			})
			resizeTable();
	}).resize();

	//
	$('.nav-link').each(function(){
		$(this).on('click', function(){
			resetImgEfect('.sucursal_header div img', 'img_logo_animation');
		});
	});

});


// funcion para efecto de carga del boton de inicio
function btnInicioSesion() {
	let elm = $('#btnEntrar');
	elm.prop("disabled", true);
  elm.html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Cargando...`);
};

// funcion para limpiar div de mensajes para una nueva transaccion
function limpiarMensajesTransaccion() {
	let divDeMensanjes = $('#mensaje_nueva_transaccion');
	divDeMensanjes.html('&nbsp;');
	divDeMensanjes.attr('class','');
	divDeMensanjes.addClass('alert');
};

// funcion para efecto de carga del boton
function btnEnviando(elm_id) {
	let elm = $(`#${elm_id}`);
	elm.prop("disabled", true);
  elm.html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Enviando...`);
};

// funcion para normalizar boton de envio
function btnEnvioNormalizar(elm_id) {
	let elm = $(`#${elm_id}`);
	elm.prop("disabled", false);
  elm.html(`Enviar`);
};

// funcion para pasar a mayuscula la primera letra de una cadena
function toSentenceCaseString(string) {
	string = string.toLowerCase();
	let result = string.charAt(0).toUpperCase() + string.slice(1);
  return result;
};

// funcion para colocar un formato de moneda a un numero
function formatCurrency(locales, currency, fractionDigits, number) {
  var formatted = new Intl.NumberFormat(locales, {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: fractionDigits
  }).format(number);
  return formatted.replace('$',currency);
};

// para dar formato a la fecha
function formatDate(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
};

// funcion limpiar formulario transaccion
function limpiarFormTransaccion() {
	$('#selector_cuenta').val('');
	$('#tipo_transaccion').prop('disabled', true).val('');
	$('#monto').prop('disabled', true).val('');
	$('#btn_enviar').prop('disabled', true);
}

// funcion para mostrar mensaje en el modal
function monstrarMensanjeModal(titulo, mensaje) {
	$('#modalLabel').html(titulo);
	$('#modalMessaje').html(mensaje);
	$('#modal_flash').modal('show');
};

// funcion para modificar el tamaño de las tablas scrollables
function resizeTable() {
	tables_scrollables_containers.each(function(){
		let container = $(this);
		let bodyCells = container.find('table.table_scrollable_header .headers th');
		let colWidth = bodyCells.map(function() {
	    return $(this).width();
		}).get();
		let table_scrollable = container.find('table.table_scrollable');
		if (table_scrollable.hasScrollBar()) {
			container.find('table.table_scrollable_header .headers td').show();
		}else{
			container.find('table.table_scrollable_header .headers td').hide();
		}
		container.find('table.table_scrollable tr:first').children().each(function(i, v) {
	    $(v).width(colWidth[i]);
		});
	}); 
};

//
function respondToVisibility(element, callback) {
  var options = {root: document.documentElement}
  var observer = new IntersectionObserver((entries, observer) => {
    entries.forEach(entry => {
      callback(entry.intersectionRatio > 0);
    });
  }, options);
  observer.observe(element);
};

//
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

//
function resetImgEfect(selector_img, classEfect) {
	var imagen = $(`${selector_img}`);
	imagen.removeClass(classEfect);
	setTimeout(() => {
		imagen.attr('class', classEfect);
	}, 500);
}