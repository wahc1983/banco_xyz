
const url = require('url');
const db_pool = require('../models/db_pool');
const Clientes = db_pool.cliente;

exports.login = function (req, res) {
		var tipo_documento = req.body.tipo_doc;
		var documento = req.body.documento;
		var password = req.body.contraseña;
    var condiciones = {clause:'WHERE tipo_documento=$1 AND documento=$2', 
                       values:[tipo_documento,documento],
                      };

    Clientes.find('*',condiciones)
    .then(result => {
      if (result.rows.length > 0 && password == result.rows[0].clave ) {
        console.log('login exitoso');
        req.session.cliente_id = `${tipo_documento}_${documento}`;
        req.body = {...result.rows[0]};
        res.redirect(307,'/sucursal_virtual');
      }else{
        var q = {
          show_error: 'true',
          title_flash: 'Error de acceso',
          flash_messaje: 'No se pudo identificar al usuario'
        }
        res.redirect(url.format({pathname:'/', query: q}));
      };      
    })
    .catch(err => {
      console.log(err);
      res.status(400).send(err);
    });
};

exports.logout = function (req, res) {
		req.session.destroy();
		res.redirect('/');
};