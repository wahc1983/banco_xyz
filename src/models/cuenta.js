const pgConector = require('../models/pg_conector').pgConector;
const tabla = 'cuenta';
const default_columns = 'fecha_creacion, saldo, tipo_documento_cliente, documento_cliente';

exports.find = function (columns, conds) {
	var query = `SELECT ${columns} FROM ${tabla}`;
	if (conds) {
		query += ` ${conds.clause}`;
		if (conds.order) {
			query += ` ${conds.order}`;
		};		
		return pgConector.query(query, conds.values);
	}else{
		return pgConector.query(query);
	};
};

exports.create = function (values, columns) {
	var query = `INSERT INTO ${tabla}`;
	(columns) ? query += ` (${columns})` : query += ` (${default_columns})`;
	query += ` VALUES (${values})`;
	query += ` RETURNING *;`;
	return pgConector.query(query);
};