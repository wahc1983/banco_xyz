const cliente = require('../models/cliente');
const cuenta = require('../models/cuenta');
const transaccion = require('../models/transaccion');
const pais = require('../models/pais');
const ciudad = require('../models/ciudad');
const telefonos_cliente = require('../models/telefonos_cliente');
const pool = module.exports;

pool.cliente = cliente;
pool.cuenta = cuenta;
pool.transaccion = transaccion;
pool.pais = pais;
pool.ciudad = ciudad;
pool.telefonos_cliente = telefonos_cliente;

