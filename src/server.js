
// creacion del servidor http
const express = require('express');
const server = module.exports;
const app = express();
const serverHttp = require('http').Server(app);
server.app = app;
server.serverHttp = serverHttp;

// Creacion del socket servidor
const serverSocketIo = require('socket.io')(serverHttp);

exports.serverSocketIo = serverSocketIo;

const socket = require('./controllers/socket_server');


