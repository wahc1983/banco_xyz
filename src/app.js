const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const favicon = require('express-favicon');
const settings = require('./settings');
const rutas = require('./routes').routes;
const session = require('express-session');
global.aplicacionHelper = require('./helpers/aplicacion');

// Acceso a los atributos de la biblioteca desde los módulos importados
var server = require('./server.js');
var app = server.app;
var serverHttp = server.serverHttp;

// Configuracion de los valores por defecto de la aplicacion
app.set('port', settings.port || 5005);
app.set('views', path.join(__dirname, 'views'));

// Configura EJS como motor por defecto para las vistas
app.set('view engine', 'ejs');

// Use the body-parser body parsing middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(favicon(path.join(__dirname, '../public/images/favicon/favicon-16x16.png')));
app.use(session({secret: 'banco_xyz', resave: true, saveUninitialized: true}));
app.use('/', rutas);

// Configurar el puerto donde la el servidor http estara escuchando
serverHttp.listen(app.settings.port, function () {
	console.log(`Server is running on Port ${app.get('port')}. Press CTRL+C to stop server.`);
});

