const express = require('express');
const rutas = module.exports;
const router = express.Router();

var sucursal_virtual = require('./controllers/sucursal_virtual');
var aplicacion = require('./controllers/aplicacion');
var inicio = require('./controllers');

router.get('/', inicio.index);
router.post('/aplicacion/login', aplicacion.login);
router.get('/aplicacion/logout', aplicacion.logout);
router.post('/sucursal_virtual', sucursal_virtual.index);

rutas.routes = router;