const url = require('url');
const db_pool = require('../models/db_pool');
const settings = require('../settings');
const Clientes = db_pool.cliente;
const Cuentas = db_pool.cuenta;
const Ciudades = db_pool.ciudad;
const Paises = db_pool.pais;
const TelefonosClientes = db_pool.telefonos_cliente;
const Transacciones = db_pool.transaccion;

exports.index = function (req, res) {
	if (req.session.cliente_id) {
		var cliente_id = req.session.cliente_id.split('_');
		var tipo_documento = cliente_id[0];
		var documento = cliente_id[1];
		var user_info, cuentas, paises, transacciones_por_cuenta = {};

		var obtener_user_info = () => {
	    let condiciones = {clause:'WHERE tipo_documento=$1 AND documento=$2', 
	                       values:[tipo_documento,documento],
	    };

	    return Clientes.find('*',condiciones);
		};
		
		var obtener_ciudad_cliente = (user_info) => {
			let condiciones = {clause:'WHERE id=$1', 
                     		 values:[user_info.id_ciudad],
      };
			return Ciudades.find('*', condiciones);
		};

		var obtener_pais_cliente = (user_info) => {
			let condiciones = {clause:'WHERE id=$1', 
                     		 values:[user_info.id_pais],
      };
			return Paises.find('*', condiciones);
		};

		var obtener_paises = () => {
			return Paises.find('*');
		};

		var obtener_telefonos_cliente = (user_info) => {
			let condiciones = {clause:'WHERE tipo_documento_cliente=$1 AND documento_cliente=$2', 
                       	 values:[user_info.tipo_documento,user_info.documento],
      };
			return TelefonosClientes.find('*', condiciones);
		};

		var obtener_cuentas_clientes = (user_info) => {
		  let condiciones = {clause:'WHERE tipo_documento_cliente=$1 AND documento_cliente=$2', 
		                     values:[user_info.tipo_documento,user_info.documento],
                     		 order: 'ORDER BY fecha_creacion DESC',
		  };
		  return Cuentas.find('*', condiciones);               
		};

		var obtener_transacciones_cuenta = (id_cuenta) => {
			let condiciones = {clause:'WHERE id_cuenta=$1',
												 values:[id_cuenta],
												 order: 'ORDER BY fecha DESC',
			};
			return Transacciones.find('*', condiciones);
		};

		var obtener_datos_usuario = (user_info) => {
			obtener_user_info().then(result => {
				user_info = {...result.rows[0]};

				if (tipo_documento == 'CC') {
					user_info['tipo_documento_cliente'] = 'Cedula de ciudadania' 
				}else if (tipo_documento == 'PA') {
					user_info['tipo_documento_cliente'] = 'Pasaporte'
				}else if (tipo_documento == 'CE') {
					user_info['tipo_documento_cliente'] = 'Cedula extranjera'
				}else if (tipo_documento == 'RC') {
					user_info['tipo_documento_cliente'] = 'Registro civil'
				}else{
					user_info['tipo_documento_cliente'] = 'NIT'
				}
				return obtener_ciudad_cliente(user_info);			
			}).then(result => {
				user_info = {...user_info, ...result.rows[0]};
				return obtener_pais_cliente(user_info);
			}).then(result => {
				user_info = {...user_info, ...result.rows[0]};
				return obtener_paises();	
			}).then(result => {
				paises = {...result.rows};
				return obtener_telefonos_cliente(user_info);	
			}).then(result => {
				user_info = {...user_info, telefonos_cliente: {...result.rows}};
				return obtener_cuentas_clientes(user_info);
			}).then(result => {
		  	cuentas = {...result.rows};
		  	transaccionesPromises = [];
		  	for (let key in cuentas) {
		  		transacciones_por_cuenta[`cuenta_${cuentas[key].id}`] = [];
		  		transaccionesPromises.push(obtener_transacciones_cuenta(cuentas[key].id));
		  	}		  	
		  	return Promise.all(transaccionesPromises)
		  }).then(result => {
		  	result.forEach(result => {
		  		let transacciones = result.rows;
		  		for (let key in transacciones) {
		  			transacciones_por_cuenta[`cuenta_${transacciones[key].id_cuenta}`].push(transacciones[key])
		  		}
		  	});
		  	render_sucursal_virtual(user_info, cuentas, transacciones_por_cuenta);
		  }).catch(err => {
		  	console.error(err);
		  	res.redirect('/');
		  });
		};

		var render_sucursal_virtual = (user_info, cuentas, transacciones_por_cuenta) => {
			res.render('sucursal_virtual/index', { 
				title: 'Sucursal Virtual',
				info_usuario: user_info,
				cuentas: cuentas,
				transacciones_por_cuenta: transacciones_por_cuenta,
				paises: paises,
				host: settings.host,
				port: settings.port,
			});			
		};

		obtener_datos_usuario(user_info);

	}else{
		res.redirect('/');
	};
};
