// Creacion del socket cliente y coneccion con el socket servidor
var hotsSocket = sessionStorage.hotsSocket;
var portSocket = sessionStorage.portSocket;

var socket = io.connect(`http://${hotsSocket}:${portSocket}`, { 'forceNew': true });

// Mensaje desde el socket servidor cuando se realiza la coneccion
socket.on('bienvenidaSocket', function(data) {
  console.log(data);
});

// funcion para manejar una nueva transaccion
function nuevaTransaccion() {
	try {
		btnEnviando('btn_enviar');
		let data = {
			id_cuenta: document.getElementById('selector_cuenta').value,
			tipo_transaccion: document.getElementById('tipo_transaccion').value,
			monto: document.getElementById('monto').value,
		};
		socket.emit('realizarTransaccion', data);
		return false;
	} catch (error) {
		btnEnvioNormalizar('btn_enviar')
	  console.error(error);
	};
};

// captura el evento respuestaTransaccion desde el servidor
socket.on('respuestaTransaccion', function(data){
	btnEnvioNormalizar('btn_enviar');
	limpiarFormTransaccion();
	var mensajeTransaccion = document.getElementById('mensaje_nueva_transaccion');	
	if (!data.error) {
		let transaccion = data.transaccion;
		let tipo_transaccion = toSentenceCaseString(transaccion.tipo_transaccion);
		let saldo_cuenta = formatCurrency("es-CO", "COP", 2, transaccion.saldo_cuenta);
		let id_cuenta = data.id_cuenta;
		let msj = '';
		let historialTransaccionesId = `historial_transacciones_cuenta_${id_cuenta}`;
		let historialTransaccionesTable = document.getElementById(historialTransaccionesId);
		let trContainer = document.createElement('tr');
		let tdTipoTrans = document.createElement('td');
		tdTipoTrans.classList.add('text-center');
		let smallTipoTrans = document.createElement('small');
		smallTipoTrans.classList.add('text-muted');
		let contentTipoTrans = document.createTextNode(`${toSentenceCaseString(transaccion.tipo_transaccion)}`);
		smallTipoTrans.appendChild(contentTipoTrans);
		tdTipoTrans.appendChild(smallTipoTrans);
		trContainer.appendChild(tdTipoTrans);
		let tdFechaTrans = document.createElement('td');
		tdFechaTrans.classList.add('text-center');
		let smallFechaTrans = document.createElement('small');
		smallFechaTrans.classList.add('text-muted');
		let contentFechaTrans = document.createTextNode(`${formatDate(new Date(transaccion.fecha), 'yyyy MM dd h:mm:ss')}`);
		smallFechaTrans.appendChild(contentFechaTrans);
		tdFechaTrans.appendChild(smallFechaTrans);
		trContainer.appendChild(tdFechaTrans);
		let tdMontoTrans = document.createElement('td');
		tdMontoTrans.classList.add('text-right');
		let smallMontoTrans = document.createElement('small');
		smallMontoTrans.classList.add('text-muted');
		let contentMontoTrans = document.createTextNode(`${formatCurrency("es-CO", "COP", 2, transaccion.monto)}`);
		smallMontoTrans.appendChild(contentMontoTrans);
		tdMontoTrans.appendChild(smallMontoTrans);
		trContainer.appendChild(tdMontoTrans);
		let tdSaldoTrans = document.createElement('td');
		tdSaldoTrans.classList.add('text-right');
		let smallSaldoTrans = document.createElement('small');
		smallSaldoTrans.classList.add('text-muted');
		let contentSaldoTrans = document.createTextNode(`${formatCurrency("es-CO", "COP", 2, transaccion.saldo_cuenta)}`);
		smallSaldoTrans.appendChild(contentSaldoTrans);
		tdSaldoTrans.appendChild(smallSaldoTrans);
		trContainer.appendChild(tdSaldoTrans);

		historialTransaccionesTable.prepend(trContainer);
		let homeSaldoCuentaId = `home_saldo_cuenta_${id_cuenta}`;
		let homeSaldoCuenta = document.getElementById(homeSaldoCuentaId);
		homeSaldoCuenta.innerHTML = `${formatCurrency("es-CO", "COP", 2, transaccion.saldo_cuenta)}`;

		mensajeTransaccion.classList.add('alert-success');
		msj += `<i class="fas fa-check-circle"></i> ${tipo_transaccion} No. ${transaccion.id} fue exitosa.`;
		msj += ` El saldo en la cuenta No. ${id_cuenta} es ${saldo_cuenta}`;
		mensajeTransaccion.innerHTML = msj;	
	}else{
		mensajeTransaccion.classList.add('alert-danger');
		msj += `<i class="fas fa-exclamation-triangle"></i> La transaccion no fue exitosa.`;
		mensajeTransaccion.innerHTML = msj;
	};
});

// funcion para comprobar el saldo en la cuenta para un retiro
function comprobarSaldo(event) {
	limpiarMensajesTransaccion();
	try {
		let data = {
			id_cuenta: document.getElementById('selector_cuenta').value,
			monto: document.getElementById('monto').value,
			event: event,
		};
		socket.emit('comprobarSaldoCuenta', data);
	} catch (error) {
		console.error(error);
	};
};

// captura el evento respuestaComprobarSaldo desde el servidor
socket.on('respuestaComprobarSaldo', function(data){

	let montoCuenta = parseInt(data.cuenta.saldo);
	let saldoSuficiente = true;

	if (data.event == 'changeSelectTransaccion') {
		if (montoCuenta < 0) {
			saldoSuficiente = false;
		};
	}else{
		let monto = document.getElementById('monto').value;
		if (monto == '') {
			monto = 0;
		}
		monto = parseInt(monto);
		let saldo = montoCuenta - monto;
		if (saldo < 0) {
			saldoSuficiente = false;
		};
	};

	if (saldoSuficiente) {
		if (data.event != 'changeSelectTransaccion') {
			document.getElementById('btn_enviar').disabled = false;
		}
	}else{
		let mensajeTransaccion = document.getElementById('mensaje_nueva_transaccion');
		mensajeTransaccion.classList.add('alert-danger');
		mensajeTransaccion.innerHTML = `<i class="fas fa-exclamation-triangle"></i>
																		Saldo insuficiente en la cuenta No. ${data.cuenta.id} para hacer un retiro`;
		document.getElementById('btn_enviar').disabled = true;
	}

});

// funcion para crear una nueva cuenta
function nuevaCuenta() {
	try {
		btnEnviando('btn_enviar_cuenta');
		let data = {
			tipo_doc_cuenta: document.getElementById('tipo_doc_cuenta').value,
			documento_cuenta: document.getElementById('documento_cuenta').value,
			saldo: document.getElementById('saldo_cuenta').value,
		};
		socket.emit('crearCuenta', data);
		return false;
	} catch (error) {
		btnEnvioNormalizar('btn_enviar');
	  console.error(error);
	};
};

// captura el evento respuestaNuevaCuenta desde el servidor
socket.on('respuestaNuevaCuenta', function(data){
	btnEnvioNormalizar('btn_enviar_cuenta');
	if (!data.error) {
		let cuenta = data.cuenta;
		document.getElementById('tipo_doc_cuenta').value = '';
		document.getElementById('documento_cuenta').value = '';
		document.getElementById('saldo_cuenta').value = '';
		let titulo_mensaje = 'Nueva cuenta';
		let mensaje = `<div class="alert alert-success" role="alert">
										<i class="fas fa-check-circle"></i>
										La cuenta No. ${cuenta.id} fue creada exitosamente.
									 </div>`;
		monstrarMensanjeModal(titulo_mensaje, mensaje);		
	}else{
		let titulo_mensaje = 'Nueva cuenta';
		let mensaje = `<div class="alert alert-danger" role="alert">
										<i class="fas fa-exclamation-triangle"></i>
										Hubo un error al crear la cuenta, verifique los datos e intente nuevamente.
									 </div>`;
		monstrarMensanjeModal(titulo_mensaje, mensaje);
	}
});

// funcion para obtener las ciudades de un pais
function obtenerCiudades(pais_id){
	socket.emit('obtenerCiudadesPais', pais_id)
};

// captura el evento mostrarCiudades desde el servidor
socket.on('mostrarCiudades', function(data){
	if (!data.error) {
		let ciudades = data.ciudades;
		let selectCiudades = document.getElementById('ciudad_nuevo');
		str = `<option value=""></option>`;
		for( let key in ciudades){
			let ciudad = ciudades[key];
			str += `<option value="${ciudad.id}">${toSentenceCaseString(ciudad.nombre_ciudad)}</option>`;
		}
		selectCiudades.disabled = false;
		selectCiudades.innerHTML = str;
	}
})

// funcion para crear cliente
function nuevoCliente() {
	btnEnviando('btn_enviar_cliente');
	let data = {
		 tipo_doc_cliente_nuevo: document.getElementById('tipo_doc_nuevo').value,
		 documento_cliente_nuevo: document.getElementById('documento_nuevo').value,
		 nombre_cliente_nuevo: document.getElementById('nombre_nuevo').value,
		 apellido_cliente_nuevo: document.getElementById('apellido_nuevo').value,
	 	 clave_cliente_nuevo: document.getElementById('contraseña_nuevo').value,
	 	 id_ciudad_cliente_nuevo: document.getElementById('ciudad_nuevo').value,
	 	 telefonos_cliente_nuevo: [],
	};

	for (var i = 1; i <= 3; i++) {
		let num_tel_cliente_nuevo = document.getElementById(`num_tel_nuevo_${i}`).value;
		if (num_tel_cliente_nuevo != '') {
				let tipo_tel_cliente_nuevo = document.getElementById(`tipo_tel_nuevo_${i}`).value;
				data['telefonos_cliente_nuevo']
				.push({tipo_tel: tipo_tel_cliente_nuevo,
							 num_tel: num_tel_cliente_nuevo,
				});
		};
	};

	console.log('data cliente nuevo: ', data);
	socket.emit('crearCliente', data);
	return false;
};

//
socket.on('respuestaClienteNuevo', function(data){
	btnEnvioNormalizar('btn_enviar_cliente');
	if (!data.error) {
		let cliente = data.cliente;
		document.getElementById('tipo_doc_nuevo').value = '';
		document.getElementById('documento_nuevo').value = '';
		document.getElementById('nombre_nuevo').value = '';
		document.getElementById('apellido_nuevo').value = '';
		document.getElementById('contraseña_nuevo').value = '';
		document.getElementById('pais_nuevo').value = '';
		document.getElementById('ciudad_nuevo').value = '';
		document.getElementById('ciudad_nuevo').disabled = true;

		for (var i = 1; i <= 3; i++) {
			document.getElementById(`num_tel_nuevo_${i}`).value = '';
			document.getElementById(`tipo_tel_nuevo_${i}`).value = '';
		};
		let titulo_mensaje = 'Nuevo cliente';
		let mensaje = `<div class="alert alert-success" role="alert">
										<i class="fas fa-check-circle"></i>
									 	El cliente identificado con ${cliente.tipo_documento} ${cliente.documento} fue creado exitosamente.
									 </div>`;
		monstrarMensanjeModal(titulo_mensaje, mensaje);
	}else{
		let titulo_mensaje = 'Nueva cliente';
		let mensaje = `<div class="alert alert-danger" role="alert">
										<i class="fas fa-exclamation-triangle"></i>
									 	Hubo un error al crear el cliente, verifique los datos e intentelo nuevamente.
									 </div>`;
		monstrarMensanjeModal(titulo_mensaje, mensaje);
	};
});

// funcion para desconectar el socket al salir
function desconectarSocket() {
	console.log('desconectar socket')
	socket.close();
	console.log(socket.disconnected);
}

