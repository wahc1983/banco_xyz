exports.index = function (req, res) {
	if (typeof req.query.show_error === "undefined") {
		res.render('index', { title: 'BANCO XYZ', show_error:'false' });
	}else{
		var title_flash = req.query.title_flash;
		var flash_messaje = req.query.flash_messaje;
		res.render('index', { title: 'BANCO XYZ', show_error:'true', title_flash: title_flash, flash_messaje: flash_messaje });
	}
};