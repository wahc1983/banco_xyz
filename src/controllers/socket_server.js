const server = require('../server');
const serverSocketIo = server.serverSocketIo;
const url = require('url');
const db_pool = require('../models/db_pool');
const Clientes = db_pool.cliente;
const Cuentas = db_pool.cuenta;
const Ciudades = db_pool.ciudad;
const Paises = db_pool.pais;
const TelefonosClientes = db_pool.telefonos_cliente;
const Transacciones = db_pool.transaccion;

// El socket servidor queda a la espera de una coneccion
serverSocketIo.on('connection', function(socket) {
  //console.log('session.cliente_id: ', session.cliente_id);
	// se lanza un mensaje por consola cuando un cliente se conecta.
  console.log(`Un cliente se ha conectado con el socket id: `, socket.id);

  // captura evento disconnect, el cual indica que un cliente se desconecto
  socket.on('disconnect', (reason) => {
    console.log('Un cliente se ha desconectado con el socket id: ', socket.id);
    console.log('Razon desconeccion: ', reason);
  });

  // se envia un mensaje por consola al cliente.
  socket.emit('bienvenidaSocket', 'Bienvenido al Banco XYZ');

  // captura el evento realizarTransaccion desde el cliente
  socket.on('realizarTransaccion', function(data) {
  	var id_cuenta = data.id_cuenta;
  	let tipo_transaccion = data.tipo_transaccion;

  	if (data.monto != '') {
  		var monto = data.monto;
  	}else{
  		var monto = 0;
  	};

  	let values = `to_timestamp(${Date.now()} / 1000.0), ${monto}, '${tipo_transaccion}', ${id_cuenta}`;

  	Transacciones.create(values).then(result => {
      let transaccion = result.rows[0];
      let condiciones = {clause: 'WHERE id=$1', values: [transaccion.id]};
      Transacciones.find('*', condiciones).then(result => {
        console.log('result transaccion: ', result.rows[0])
        let data = {transaccion: result.rows[0], id_cuenta: id_cuenta}
        socket.emit('respuestaTransaccion', data);
      }).catch(err => {
        console.error(err);
        let data = {error: err};
        socket.emit('respuestaTransaccion', data);      
      });
  	}).catch(err => {
  		console.error(err);
      let data = {error: err};
      socket.emit('respuestaTransaccion', data);      
  	});
  });

  // captura el evento comprobarSaldoCuenta desde el cliente
  socket.on('comprobarSaldoCuenta', function(data){
    let id_cuenta = data.id_cuenta;
    let event = data.event;

    if (data.monto != '') {
      let monto = data.monto;
    }else{
      let monto = 0;
    };

    let condiciones = {clause: 'WHERE id=$1', values: [id_cuenta]};    

    Cuentas.find('*', condiciones).then(result => {
      let cuenta = result.rows[0];
      let data = {cuenta: cuenta, event: event};
      socket.emit('respuestaComprobarSaldo', data);
    }).catch(err => {
      console.error(err);
      let data = {error: err};
      socket.emit('respuestaComprobarSaldo', data);    
    });
  });

  // captura el evento para crear una cuenta
  socket.on('crearCuenta', function(data){
    let tipo_doc = data.tipo_doc_cuenta;
    let documento = data.documento_cuenta;
    let saldo = data.saldo;

    let values = `to_timestamp(${Date.now()} / 1000.0), ${saldo}, '${tipo_doc}', ${documento}`;

    Cuentas.create(values).then(result => {
      let cuenta = result.rows[0];
      let data = {cuenta: cuenta};
      socket.emit('respuestaNuevaCuenta', data)
    }).catch(err => {
      console.error(err);
      let data = {error: err};
      socket.emit('respuestaNuevaCuenta', data);     
    });
    
  });

  // captura el evento para enviar las ciudades de un pais
  socket.on('obtenerCiudadesPais', function(data){
    let id_pais = data;
    let condiciones = {clause: 'WHERE id_pais=$1', values: [id_pais]};
    Ciudades.find('*', condiciones).then(result => {
      let ciudades = result.rows;
      let data = {ciudades: ciudades};
      socket.emit('mostrarCiudades', data);
    }).catch(err => {
      console.error(err);
      let data = {error: err};
      socket.emit('mostrarCiudades', data);     
    });
  });

  // captura el evento para crear un cliente
  socket.on('crearCliente', function(data){

    let tipo_documento = data.tipo_doc_cliente_nuevo;
    let documento = data.documento_cliente_nuevo;
    let nombre = data.nombre_cliente_nuevo;
    let apellido = data.apellido_cliente_nuevo;
    let id_ciudad = data.id_ciudad_cliente_nuevo;
    let clave = data.clave_cliente_nuevo;
    var telefonos_cliente = data.telefonos_cliente_nuevo;

    let values = `'${tipo_documento}', '${documento}', '${nombre}', '${apellido}', ${id_ciudad}, '${clave}'`;

    Clientes.create(values)
    .then(result => {
      var cliente = result.rows[0];
      console.log('cliente nuevo: ', cliente)
      if (telefonos_cliente.length != 0) {
        crearNuevosTelefonos(cliente, telefonos_cliente);
      }else{
        let data = {cliente: cliente};
        socket.emit('respuestaClienteNuevo', data);
      }
    }).catch(err => {
      console.error(err);
      let data = {error: err};
      socket.emit('respuestaClienteNuevo', data);
    });
  });

  // funcion para insertar telefonos del cliente
  function crearNuevosTelefonos(cliente, telefonos) {
    var cliente = cliente;
    var tipo_documento_cliente = cliente.tipo_documento;
    var documento_cliente = cliente.documento;
    var promesas_telefonos = [];

    for( let key in telefonos){
      let telefono = telefonos[key];
      let numero_telefonico = telefono.num_tel;
      let tipo_telefono = telefono.tipo_tel;
      let values = `${numero_telefonico}, '${tipo_documento_cliente}', '${documento_cliente}', '${tipo_telefono}'`;
      promesas_telefonos.push(TelefonosClientes.create(values));
    }
    Promise.all(promesas_telefonos)
    .then(result => {
      console.log('nuevos telefonos: ',result.rows);
      let data = {cliente: cliente}
      socket.emit('respuestaClienteNuevo', data);
    }).catch(err => {
      console.error(err);
    });
  };

});

